#getval_hp_g3
#created by _KUL (Kuleshov)
#version 2016-07-05
#Apache License 2.0

#!/usr/bin/perl
use warnings;
use strict;
use LWP::UserAgent;
use Digest::MD5 qw(md5_hex);
use XML::Simple;
use JSON;
use Switch;
use v5.14;

#Описание свойств для использования
#https://support.hpe.com/hpsc/doc/public/display?docId=emr_na-c02520779&docLocale=en_US#G5.435650

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

if (scalar @ARGV ne 4) { die "Need to run so: getval_hp_g3.pl <IP[orIP]> <functionname> <IDparam1> <NAMEparam2>\n---\nExample: getval_hp_g3.pl 172.17.0.1or172.17.0.2 system 1 health\n---\nDocs: https://support.hpe.com/hpsc/doc/public/display?docId=emr_na-c02520779&docLocale=en_US#G5.435650\n";}

#выбираем случайный ип между or в строке
my @tmp = split(/or/,$ARGV[0]);
my $hostname = $tmp[int(rand(scalar @tmp))];

#учётные данные для авторизации на полке
my $USERNAME = "Zabbix1";
my $PASSWORD = "Zabbix27";

my $md5_hash = md5_hex("${USERNAME}_${PASSWORD}");
my $funct;
my %options = (
	ssl_opts => {
		verify_hostnames => 0,
		SSL_verify_mode => 0x00,
	}
);

my $ua = LWP::UserAgent->new(%options);
my $url = "https://$hostname/api/login/" . $md5_hash; 
my $req = HTTP::Request->new(GET => $url);
my $res = $ua->request($req);

my $ref = XMLin($res->content); 
my $sessionKey;

if (exists $ref->{OBJECT}->{PROPERTY}->{"return-code"}->{content} == 1) { 
	$sessionKey = $ref->{OBJECT}->{PROPERTY}->{"response"}->{content};
} else {
    die($ref->{OBJECT}->{PROPERTY}->{"response"}->{content});
}

$funct = sub {
	#урл функции
    my $url = "https://$hostname/api/show/" . shift;
	#ид корпуса
    my $id = shift;
    #имя параметра
    my $param = shift;
    my $req = HTTP::Request->new(GET => $url);
    $req->header('sessionKey' => $sessionKey );
    $req->header('dataType' => 'api' );
    my $res = $ua->request($req);
    my $ref = XMLin($res->content,KeyAttr => "oid");
    my $result = "";
    foreach my $str (@{$ref->{OBJECT}->{$id}->{PROPERTY}}) {
		if ($str->{name} eq $param) {
            if ($str->{name} eq "health") {
				switch ($str->{content}) {
					case "OK" {$result = 0}
					case "Degraded" {$result = 1}
					case "Fault" {$result = 2}
					case "Unknown" {$result = 3}
					case "N/A" {$result = 4}
					else {$result = -1}
				}
            } else {
                $result = $str->{content};
            }
        }
    }
    say $result;
};

$funct->($ARGV[1],$ARGV[2],$ARGV[3]);
